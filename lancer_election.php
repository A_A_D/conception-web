<?php session_start();
include('Verif_session_admin.php');
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">

  <title>Lancer une élection</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_sheet.css">
</head>
    <body style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
        <div class="row  Dblue">
            <div class="container-fluid d-flex justify-content-center">
                <div class="d-flex justify-content-center offset-md-1 col-8"><h4 class="my-3 text-center" style=" color: whitesmoke">Vote ton ping.</h4> </div> 
                <div class=" justify-content-end"><a class="btn btn-custom my-3 r" href="index.php" >Déconection</a></div>
            </div>
           
        </div>
        <div class="col-12">
            <div  class="row justify-content-center">

                <div>
                       <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                           <li class="nav-item">
                            <a class="nav-link active " href="page_accueil_utilisateur.php" >Accueil</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active " href="page_poster.php" >Visualiser Poster</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?> " href="creation_sujet.php">Création d'un sujet</a>
                          </li>
                           <li class="nav-item">
                            <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?> " href="gestion_election.php">Gestion de l'élection</a>
                          </li>
                        </ul>
                </div> 
            </div>
            <div class="row ml-md-2 mx-auto">
             <div class="   justify-content-center shadow-sm col-md-2   col-12 h-75  Dblue my-3" style="width: 100%; border-radius: 7px">
                <article class=" mx-4 my-2 border-bottom border-white ">
                    <h4 class="px-5 m-2 py-2 d-flex justify-content-center"> <u> Profil </u></h4>
                    <div class=" mx-1  col-5 col-md-12 justify-content-center ">
                        <img src ="<?php echo $_SESSION['photo_profil']; ?>" style="height: 100px; width: 100px" alt="<?php $_SESSION['id'];?>">
                        <h5 > <b> pseudo: </b> <?php echo $_SESSION['pseudo']. '<br />'; ?></h5>
                    </div>
                    
                </article>
                 <article class=" mx-4 my-2" style="opacity: 1"><h2 >ESIGELEC</h2>
                        <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                        </p>
                        <a href="http://www.esigelec.fr/en" style="color:white"> * visiter le site de l'ESIGELEC >> </a>
                    </article>
                </div>
                
                <div id="container" class=" col-12 col-md-9 container gray ml-1 mt-3  rounded shadow" style="height: 100%">
                    <div class="row my-4 pt-5  offset-md-2 col-md-8 col-12">
                        <div class="d-flex   justify-content-end border-right border-info ">
                             <form action="lance_election.php" style="width: 400px" method="post">
                                  <div class=" form-group ">
                                      <h2 style="align-content: center">Lance une élection</h2>
                                  </div>
                                <div class=" form-group ">
                                    <label for="debut">Date de début:</label>
                                    <input type="text" id="debut" class="form-control" name="debut" placeholder="YYYY-mm-dd" style="width: auto">
                                  </div>
                                 <div class=" form-group ">
                                    <label for="fin">Date de fin:</label>
                                    <input type="text" id="fin" class="form-control" name="fin" placeholder="YYYY-mm-dd" style="width: auto">
                                  </div>
                                  <button type="submit" class="btn btn-primary my-2 mx-auto">Créer</button>
                             </form>
                        </div>
                    <div class="d-flex align-items-center col-12 col-md"><h5>Veuillez renseignez ces champs pour démarrer une nouvelle élection.</h5>
                    </div>
                </div>

                </div>

            </div>
        </div>
    </body>
</html>
