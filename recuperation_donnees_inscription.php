
<?php
    session_start();
  
if(isset($_POST['Email1'] )&&isset($_POST['pseudo'] )&&isset($_POST['exampleInputPassword1'] )&&isset($_POST['exampleInputPassword2'] ))
{
if($_POST['exampleInputPassword2']!= $_POST['exampleInputPassword1'])
{   
    $_SESSION['erreur_mdp']="erreur";
    header("Location:page_inscription.php");
}
else
{
    try
    {
       include ("connexion_database.inc.php");
        //préparation de la requête SQL
        $requete = $objet_PDO -> prepare('SELECT user_id FROM usr where user_id=?');
        $requete->execute(array($_POST['Email1']));
        if($donnees=$requete->fetch())
        {
            $_SESSION['erreur_user']="erreur";
            header("Location:page_inscription.php");
        }
        else
        {
            try{
                $pass = password_hash($_POST['exampleInputPassword1'], PASSWORD_DEFAULT);
                $requete = $objet_PDO -> prepare('insert into usr values(:id,:vote,:mdp,:status,:Pseudo,:pic)');
            $requete ->execute(array (
                              'id'=> $_POST['Email1'],
                              'vote' => null,
                              'mdp' =>$pass,
                              'status' => 0,
                              'Pseudo' => $_POST['pseudo'],
                              'pic'=>"profile.jpg"
                            ));
            }
             catch (Exception $e)
            {
                    die('Erreur : ' . $e->getMessage());
            }
        }
        
        
    }
    catch (Exception $e)
    {
            die('Erreur : ' . $e->getMessage());
    }

   ?> 
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
<meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
  <title>Vote_ton_ping</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_sheet.css">
</head>
    <body id="container" style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
        <div class="row  Dblue">
            <div class="container-fluid d-flex justify-content-center">
                <div class="d-flex justify-content-center offset-md-1 col-8"><h4 class="my-3 text-center" style=" color: whitesmoke"><p >Vote ton ping.</p></h4> </div> 
                <div class=" justify-content-end"><a class="btn btn-custom my-3" href="page_connexion.php" >connexion</a></div>
            </div>
           
        </div>
        <div class="col-12">
            <div  class="row justify-content-center">

                <div>
                       <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                          <li class="nav-item">
                            <a class="nav-link active " href="page_connexion.php" >Connexion</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link " href="page_inscription.php">Inscription</a>
                          </li>
                        </ul>
                </div> 
            </div>
        </div>
            <div  class="row mx-auto ">
                    <div class="  shadow-sm col-md-2  col-sm-12 col-12 h-75  Dblue ml-md-2 my-3" style="width: 100%; border-radius: 7px">
                        <article class=" mx-4 my-2" style="opacity: 1"><h2 ><p>ESIGELEC</p></h2>
                            <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                            </p>
                            <a href="http://www.esigelec.fr/en" style="color:white"> * visiter le site de l'ESIGELEC >> </a>
                        </article>
                    </div>

                    <div id="container" class=" col-12 col-md-9 container gray ml-1 mt-3  rounded shadow" style="height: 100%">
                        <div class="row my-4 pt-5  offset-md-2 col-md-8 col-12">
                            <div class="d-flex flex-wrap align-items-center mx-auto my-auto">
                                 <h4>Félicitations!</h4><br />
                                 <h5 class="card-subtitle mb-2 text-muted"> Vous vous êtes inscrit avec succes</h5>
                                <article>
                                    <p style="font-size: 16px">vous pouvez maintenant vous participer à l'élection il suffit de vous <a href="page_connexion.php" > connecter </a></p>
                                    <p class="card-text"> <a href="index.php"> revenir à la page d accueil </a></p>
                                </article>
                            </div>
                        </div>

                     </div>
                </div>
    </body>
</html>
<?php } }
else {
    header("location:index.php");
}
?>