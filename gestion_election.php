
<?php
    session_start();
include('Verif_session_admin.php');
    include("connexion_database.inc.php");    
        ?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gestion election</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_sheet.css">
</head>
    <body style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
        <div class="row Dblue">
            <div class="container-fluid d-flex justify-content-center">
                <div class="d-flex justify-content-center offset-md-1 col-8"><h3 class="my-2 text-center" style=" color: whitesmoke">Vote ton ping.</h3> </div> 
                <div class=" justify-content-end">
                    <a href="#" class="mr-3" style="color: white;">Profil</a>
                    <a class="btn btn-custom my-3 r" href="index.php" >Déconnection</a></div>
            </div>
        </div>
        
        <div class="col-12">
            <div  class="row justify-content-center">

                <div>
                       <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                           <li class="nav-item">
                            <a class="nav-link active " href="page_accueil_utilisateur.php" >Accueil</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active " href="page_poster.php" >Visualiser Poster</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?>" href="creation_sujet.php">Création d'un sujet</a>
                          </li>
                           <li class="nav-item">
                            <a class="nav-link  disabled" href="#">Gestion de l'élection</a>
                          </li>
                        </ul>
                </div> 
            </div>
            <div class="row ml-md-2 mx-auto">
             <div class="   justify-content-center shadow-sm col-md-2   col-12 h-75  Dblue my-3" style="width: 100%; border-radius: 7px" >
                <article class=" mx-4 my-2 border-bottom border-white ">
                    <h4 class="px-5 m-2 py-2 d-flex justify-content-center"> <u> Profil </u></h4>
                    <div class=" mx-1  col-5 col-md-12 justify-content-center ">
                        <img src ="<?php echo $_SESSION['photo_profil']; ?>" style="height: 100px; width: 100px" alt="<?php $_SESSION['id'];?>">
                        <h5 > <b> pseudo: </b> <?php echo $_SESSION['pseudo'].'<br/>'; ?> </h5>
                    </div>
                    
                </article>
                 <article class=" mx-4 my-2" style="opacity: 1"><h2>ESIGELEC</h2>
                        <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                        </p>
                        <a href="http://www.esigelec.fr/en" style="color:white"> * visiter le site de l'ESIGELEC >> </a>
                    </article>
                </div>
                
                <div id="container" class=" col-12 col-md-9 container gray ml-1 mt-3  rounded shadow" style="height: 100%">
                    <div class="row my-4 pt-5  offset-md-2 col-md-8 col-12">
                        <div class="d-flex   justify-content-end border-right border-info form-group">
                                 <?php  $requete1 = $objet_PDO -> prepare('SELECT * FROM election WHERE statut = ?');
                                        $requete1->execute(array(1));
                                        $tuples = $requete1->fetchAll();
                                        $test = count($tuples);

                                        if($test == 1){?>
                                        
                                    <div class=" form-group  mr-1">
                                      <h2 style="align-content: center">Election en cours</h2>
                                    <label for="auteur">
                                        <?php foreach ($tuples as $tuple): ?>
                                            <p> Date début: <?= $tuple['date_debut'] ?> </p>
                                            <p> Date fin: <?= $tuple['date_fin'] ?></p>
                                        <?php endforeach;?> </label>
                                      <div>
                                            <button class="btn btn-primary my-2 mx-auto disabled" >Lancer une élection</button>
                                            <a href="cloturer_election.php"><button type="submit" class="btn btn-primary my-2 mx-auto">Clôturer l'élection en cours</button></a>
                                        </div>
                                    <?php } else { ?>
                                        <div class=" form-group mr-2">
                                      <h2 style="align-content: center">Aucune élection en cours</h2>
                                    <label>Veuillez lancer une élection en cliquant sur le bouton ci-dessous.</label>
                                      <div>
                                            <a href="lancer_election.php" class="btn btn-primary my-2 mx-auto " >Lancer une élection </a>
                                            <button type="submit" class="btn btn-primary my-2 mx-auto disabled">Clôturer l'élection en cours</button>
                                        </div>
                                            <?php } ?>
                                        
                                            
                                </div>
                                
                                 
                                  
                        </div>
                    <div class="d-flex align-items-center col-12 col-md-3"><h5>Vous trouverez ici des détails et des actions pour la gestion d'élection.</h5>
                    </div>
                </div>

                </div>

            </div>
        </div>
    </body>
</html>
