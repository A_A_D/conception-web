<?php 
session_start(); 
?>
<?php
if(isset($_POST['Email1']) && isset($_POST['Password1']))
{
     $_SESSION=$_POST;
}   
if(isset($_SESSION['Email1']) && isset($_SESSION['Password1']))
{     
try
{
	// On se connecte à MySQL
    include ("connexion_database.inc.php");
    $req = $objet_PDO->query('SELECT user_mdp,user_id FROM usr');
   
    while($donnee = $req->fetch())
    {
        if(password_verify($_SESSION['Password1'], $donnee['user_mdp']))
        {
                if($_SESSION['Email1'] == $donnee['user_id'])
                    {
                    
                        $_SESSION['Password1'] = $donnee['user_mdp'];
                    }
        }
    }
    $requete = $objet_PDO->prepare('SELECT * FROM usr WHERE user_id=? AND user_mdp=?');
    $requete->execute(array($_SESSION['Email1'], $_SESSION['Password1']));
    
    if($donnees = $requete->fetch()) {
        $_SESSION['photo_profil']=$donnees['user_picture'];
        $_SESSION['pseudo']=$donnees['user_pseudo'];
        $_SESSION['vote'] = $donnees['user_vote'];
        $_SESSION['statut'] = $donnees['user_statut'];
        $_SESSION['date'] = date('Y-m-d');
        
        $requete1 = $objet_PDO -> prepare('SELECT * FROM election WHERE statut = ?');
        $requete1->execute(array(1));
        $tuple = $requete1->fetchAll();
        
        if(!empty($tuple[0])){
            $_SESSION['election']=$tuple[0]['id_election'];

            if($tuple[0]['date_fin'] < $_SESSION['date']){
                $requete2 = $objet_PDO->prepare('UPDATE election SET statut=? WHERE statut = ?'); 
                $requete2 ->execute(array(1,0));
            }
        }
?>
    <!doctype html>
    <html lang="fr">
        <head>
          <meta charset="utf-8">
	  <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">

          <title>Vote_ton_ping</title>
            <!-- css -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/style_sheet.css">
        </head>
            <body  style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
               <div class="row Dblue">
                    <div class="container-fluid d-flex justify-content-center">
                        <div class="d-flex justify-content-center offset-md-1 col-8"><h4 class="my-2 text-center" style=" color: whitesmoke">Vote ton ping.</h4> </div> 
                        <div class=" justify-content-end">
                            <a href="#" class="mr-3" style="color: white;">Profil</a>
                            <a class="btn btn-custom my-3 r" href="index.php" >Déconnection</a></div>
                    </div>

                </div>
                <div class="col-12">
                    <div  class="row justify-content-center">

                        <div>
                               <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                                  <li class="nav-item">
                                    <a class="nav-link disabled" href="page_accueil_utilisateur.php" >Accueil</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link active" href="page_poster.php">Visualisation des posters</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?>" href="creation_sujet.php">Création d'un sujet</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?>" href="gestion_election.php">Gestion de l'élection</a>
                                  </li>
                                </ul>
                        </div> 
                    </div>
                </div>
        <div class="row ml-md-2 mx-auto">
             <div class="   justify-content-center shadow-sm col-md-2   col-12 h-75  Dblue my-3" style="width: 100%; border-radius: 7px">
                <article class=" mx-4 my-2 border-bottom border-white ">
                    <h4 class="px-5 m-2 py-2 d-flex justify-content-center"> <u> Profil </u></h4>
                    <div class=" mx-1  col-5 col-md-12 justify-content-center ">
                        <img src ="<?php echo $_SESSION['photo_profil']; ?>" style="height: 100px; width: 100px" alt="image">
                        <h5 > <b> Pseudo: </b> <?php echo $_SESSION['pseudo']. '<br />'; ?></h5>
                    </div>
                    
                </article>
                 <article class=" mx-4 my-2" style="opacity: 1"><h2 >ESIGELEC</h2>
                        <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                        </p>
                        <a href="http://www.esigelec.fr/en" style="color:white"> * visiter le site de l'ESIGELEC >> </a>
                    </article>
                </div>
             <div id="container" class=" col-12 col-md-8 container gray ml-1 mt-3 pt-5 rounded shadow  align-items-center" style="height:100%">
                    <div class="d-flex align-items-center ">
                        <div class="row align-items-center">
                             <img class="col-12 col-md-4 my-3 justify-content-center rounded" src="nesa-by-makers-kwzWjTnDPLk-unsplash.jpg" alt="image ping">
                             <article class="col-12 col-md-8   my-2 text-center">
                                <h1>Le <b>PING</b> qu'est ce que c'est ?</h1><br />
                                <p>PING signifie <b>projet ingénieur</b></p>
                                <p>il s'agit d'un projet fourni par un client et dont le but est de former les étudiants de troisieme année à la gestion et à la réalisation d'un projet technique dans tous ses aspects: budjet, deadlines,humanitaires...</p>
                             <p>vous pouvez décider de supporter votre équipe favorite en votant pour son projet! il vous suffit de cliquer sur le poster qui vous intérresse et de voter sur sa page!</p>
                             <p> vous pouvez à présent <a href="page_poster.php"> afficher la liste des posters </a>pour faire votre choix</p>
                             <a class="btn btn-primary b my-3 " href="page_poster.php"> supportez votre projet! </a>
                        </article>
                        </div>
                </div>
            </div>
        </div>
        </body>
        </html>
        <?php } else {
                $_SESSION['erreur_mdp_co']="erreur";
                header("Location:page_connexion.php");
    } ?>
<?php }
catch(Exception $e)
{
	// En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}
}
else
{
    header("Location:page_connexion.php");
}?>