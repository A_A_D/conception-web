<!DOCTYPE html>
<?php include 'recueillir_BDD.inc.php' ;?>

<html>
    <head>
        <meta charset="utf-8">
        <title>Connexion</title>
                
        <!-- Bootstrap & CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="stylesheet_AAD.css">
        
        <!-- Responsive meta tag - BootSTRAP -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
    </head>
    
    <body>
        <header class="col-12 col-md-12 col-xl-12">
            <div>
                <img src="banniere.jpg" alt="innovation" class="img-fluid">
            </div>
            <div class="row justify-content between menu">
                <div align="left" class="col-10 col-md-10 col-xl-10">
                    <a href="page_accueil.html" class="btn btn-light active" role="button" aria-pressed="true">Accueil</a>
                    <a href="page_accueil.html" class="btn btn-light active" role="button" aria-pressed="true">Visualiser les posters</a>
                    <a href="page_accueil.html" class="btn btn-light active" role="button" aria-pressed="true">Création d'un sujet</a>
                    <a href="page_accueil.html" class="btn btn-light active" role="button" aria-pressed="true">Gestion de l'élection</a>
                </div>
                <div align="right" class="col-2 col-md-2 col-xl-2" >
                    <a href="page_accueil.html" class="btn btn-light active" role="button" aria-pressed="true">Déconnexion</a>
                </div>

            </div>
            
        </header>
        
        <div class="col-12 col-md-12 col-xl-12 row " id="marge">
            <!-- A propos de l'ESIGELEC -->
            <div class="col-12 col-md-3 col-xl-3 bloc">
                <h2>ESIGELEC</h2>
                <p> L'ESIGELEC est l'une des 205 écoles d'ingénieurs françaises accréditées au 1er septembre 2018 à délivrer un diplôme d'ingénieur3. École consulaire, sous tutelle du ministère de l'Enseignement Supérieur et de la Recherche, elle a été créée en 1901, contractualisée avec l’État, associée au Groupe des Écoles des Mines, installée à Rouen depuis 1978, soutenue par la chambre de commerce et d'industrie de Rouen (CCIR) qui la cogère dans le cadre d’une association loi de 1901 avec l'ESIGELEC-Alumni (Anciennement Société des Ingénieurs en Génie Électrique - SIGELEC) regroupant les anciens élèves.</p>
            </div>
            
            <div class="col-12 col-md-2 col-xl-1 ">
                
            </div>
            
            <!-- A propos du PING -->
            <div class="col-12 col-md-7 col-xl-8 bloc">
                <h2 align="center" class="margeinterieure">Connexion</h2>
                
                <form action="recup_donnees.inc.php"  method="post">
                    <div align="center">
                        <label for="identifiant">
                            <h4>Identifiant</h4>
                        </label>
                    </div>
                    <div class="margeinterieure" align="center">
                        <input type="email" placeholder="exemple@exemple.com" name="identifiant" required>
                    </div>
                    <div align="center">
                        <label for="mdp">
                            <h4>Mot-de-passe</h4>
                        </label>
                    </div>
                    <div class="margeinterieure" align="center">
                        <input type="password" placeholder="********" name="mdp" required>
                    </div> 

                    <input type="submit" value="Se connecter">
                </form>
            </div>            
            
        </div>
        
    </body>
    
</html>