<?php

    try
    {
       include ("connexion_database.inc.php");
        //préparation de la requête SQL
        try{
            $requete1 = $objet_PDO -> prepare('SELECT * FROM election WHERE statut = ?');
            $requete1->execute(array(1));
            $test = count($requete1->fetchAll());
            if(!$requete1){
                echo "Mauvaise requete";
            }else if($test== 0){
                echo "Aucune élection en cours";
            }else {
                
                $requete3 = $objet_PDO -> prepare('SELECT projet.projet_auteur, usr.user_vote,COUNT(usr.user_vote) FROM usr INNER JOIN projet ON usr.user_vote=projet.projet_id GROUP BY usr.user_vote ORDER BY COUNT(usr.user_vote) DESC');
                $requete3->execute(array(1));
                while($tuples = $requete3->fetch()){
                    $requete4 = $objet_PDO -> prepare('UPDATE projet SET projet_votes=? WHERE projet_id = ?'); 
                    $requete4->execute(array($tuples['COUNT(usr.user_vote)'],$tuples['user_vote']));
                }
                    
                
                $requete2 = $objet_PDO -> prepare('UPDATE election SET statut=?, date_fin=? WHERE statut = ?'); 
                $requete2->execute(array(0,date('Y-m-d'),1));
                header('Location:gestion_election.php');
            }
                
        }
         catch (Exception $e)
        {
                die('Erreur : ' . $e->getMessage());
        }
        
        
    }
    catch (Exception $e)
    {
            die('Erreur : ' . $e->getMessage());
    }

   ?> 