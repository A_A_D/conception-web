<?php session_start(); 
include('verif_session.php');
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
  <title>Vote_ton_ping</title>
    <!-- js -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_sheet.css">
</head>
<body id="container" style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
        <div class="row  Dblue">
            <div class="container-fluid d-flex justify-content-center">
                <div class="d-flex justify-content-center offset-md-1 col-8"><h4 class="my-3 text-center" style=" color: whitesmoke"><p >Vote ton ping.</p></h4> </div> 
                <div class=" justify-content-end">
                            <a href="#" class="mr-3" style="color: white;">Profil</a>
                            <a class="btn btn-custom my-3 r" href="index.php" >Déconnection</a></div>
            </div>
        </div>
        <div class="col-12">
            <div  class="row justify-content-center">

                <div>
                       <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                                  <li class="nav-item">
                                    <a class="nav-link active " href="page_accueil_utilisateur.php" >Accueil</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link active" href="page_poster.php">Visualisation des posters</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?>" href="creation_sujet.php">Création d'un sujet</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?>" href="gestion_election.php">Gestion de l'élection</a>
                                  </li>
                                </ul>
                </div> 
            </div>
        </div>
            <div  class="row mx-auto">
                 <div class="   justify-content-center shadow-sm col-md-2   col-12 h-75  Dblue my-3 ml-2" style="width: 100%; border-radius: 7px">
                <article class=" mx-4 my-2 border-bottom border-white ">
                    <h4 class="px-5 m-2 py-2 d-flex justify-content-center"> <u> Profil </u></h4>
                    <div class=" mx-1  col-5 col-md-12 justify-content-center ">
                        <img src ="<?php echo $_SESSION['photo_profil']; ?>" style="height: 100px; width: 100px">
                        <h5 > <b> pseudo: </b> <?php echo $_SESSION['pseudo']; echo "<br />"; ?></h5>
                    </div>
                    
                </article>
                    <article class=" mx-4 my-2" style="opacity: 1"><h2 ><p>ESIGELEC</p></h2>
                        <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                        </p>
                        <a href="http://www.esigelec.fr/en" style="color:white"> * visiter le site de l'ESIGELEC >> </a>
                    </article>
                </div>
    
        <!-- le corps-->
             <div id="container" class=" col-12 col-md-9 container gray ml-1 mt-3  rounded shadow" style="height: 100%">
                <div class="row">
                    <h2 class="offset-md-5 offset-xs-4 my-4" style="text-align: center"> Projet n° <?php echo $_GET['id'] ?></h2><br />
                </div>
                 <?php if(isset($_GET['image']) && isset($_GET['id']) && isset($_GET['image']) && isset($_GET['auteur']) && isset($_GET['description'])) { ?>
                    <div class="offset-md-2 my-2 d-flex flex-wrap align-items-center">
                        
                        <div class="col-md-5 col-12">
                            <img src="<?php echo $_GET['image']; ?> "alt="poster_1" class=" border-info border-right   col-md-12 mb-2 ">
                        </div>
                         <div class="card col-md-5 col-12 shadow-sm" style="width: 18rem;">
                             <h3 class="card-Title"><?php echo "projet " . $_GET['id']; 
                                 $id_pro = $_GET['id'];  ?></h3>
                             <article class=" card-body col-md-12">
                                 <?php 
                                  try
                                {
                                    // On se connecte à MySQL
                                    include ("connexion_database.inc.php");

                                    $requete = $objet_PDO->prepare('SELECT user_pseudo FROM usr WHERE user_id = ? ');
                                    $requete->execute(array($_GET['auteur']));
                                    if($donnees = $requete->fetch()) {
                                        $auteur['pseudo'] = $donnees['user_pseudo'];
                                    }
                                      else
                                      {
                                          $auteur['pseudo']="Pseudo non défini";
                                      }
                                  }
                                  catch(Exception $e)
                                {
                                    // En cas d'erreur, on affiche un message et on arrête tout
                                        die('Erreur : '.$e->getMessage());
                                }   
                                ?>                                                                                              
                                                                                                                              
                                 <p style="text-align: left" class="my-1"><b>auteur: </b><?php echo $auteur['pseudo']; ?></p>
                                 <p><b>résumé du projet :</b><?php echo $_GET['description']; ?></p>
                                 
                                     
                                 <button  class="btn btn-primary my-2 b " style="width: 150px; align-self: center" data-toggle="modal" data-target="#exampleModalCenter" id="bouton_voter"   <?php if($_SESSION['vote']!= null)
                                {
                                    echo 'disabled';
                                }?>>Voter!
                                     </button>
                                
                             </article>
                             <?php if(isset($_SESSION['erreur_vote']) && $_SESSION['erreur_vote']!= null )
                                { ?>
                                    <div class="alert alert-danger"> <?php echo $_SESSION['erreur_vote'];
                                        $_SESSION['erreur_vote']=null; ?></div>
                               <?php } ?>
                        </div>                        
                       
                    </div>
                 <?php } ?>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content" id="mod" style="background:rgba(19,97,142,0.7);">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle" style="color:whitesmoke;">Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" style="color:whitesmoke;">
                           Voulez vous confirmer votre vote pour le projet <?php echo $_GET['id'] ?> ?<hr style="color:whitesmoke;"> - Cliquez sur "valider" pour confirmer le vote (vous serez redirigés vers la page de presentation des poster)<br/>
                           - Cliquez sur annuler pour annuler le vote
                          </div>
                          <div class="modal-footer">
                              <form method="post" action="voter.php">
                                 <input type="hidden" id="projet_id" name="projet_id" value=" <?php echo $id_pro; ?> "> 
                                <input type="submit" value="valider" class="btn btn-custom" >
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>

    </div>
</body>
</html>