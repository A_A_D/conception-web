<?php session_start();

?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
  <title>Vote_ton_ping</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_sheet.css">
    <script src="bootstrap.min.js"></script>
    <script src="popper.min.js"></script>
    <script src="js/Mes_Script.js"></script>
    <!-- <link rel="" -->
</head>
    <body  style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
        <div class="row  Dblue">
            <div class="container-fluid d-flex justify-content-center shadow-bottom">
                <div class="d-flex justify-content-center offset-md-1 col-8"><h4 class="my-3 text-center" style=" color: whitesmoke"><a href="index.php" class="text-decoration-none" style="color:whitesmoke; ">Vote ton ping.</a></h4> </div> 
                <div class=" justify-content-end"><a class="btn btn-custom my-3" href="page_connexion.php" >Connexion</a></div>
            </div>
           
        </div>
        <div class="col-12">
            <div  class="row justify-content-center">

                <div>
                       <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                          <li class="nav-item">
                            <a class="nav-link active " href="page_connexion.php" >Connexion</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link " href="page_inscription.php">Inscription</a>
                          </li>
                        </ul>
                </div> 
            </div>
            <div  class="row mx-auto">
                <div class="  shadow-sm col-md-2  col-sm-12 col-12 h-75  Dblue ml-md-2 my-3" style="width: 100%; border-radius: 7px">
                    <article class=" mx-4 my-2" style="opacity: 1"><h2 >ESIGELEC</h2>
                        <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                        </p>
                        <a href="http://www.esigelec.fr/en" style="color:white"> * Visiter le site de l'ESIGELEC >> </a>
                    </article>
                </div>
                
                <div id="container" class=" col-12 col-md-9 container gray ml-1 mt-3 mb-2 rounded shadow" style="height: 100%">
                    <div class="row my-4 pt-5  offset-md-2 col-md-8 col-12">
                        <div class="d-flex flex-wrap align-items-center mx-auto my-auto">
                         <form class=" col-10 col-md p-l-3  border-right border-info" action="recuperation_donnees_inscription.php" style="width: 400px" method="post" id="formulaire1" >
                              <div class="form-group ">
                                  <h2 style="align-content: center">Inscription</h2>
                                <label for="exampleInputEmail1">Adresse mail</label>
                                <input required type="email" class="form-control" name="Email1" aria-describedby="emailHelp" placeholder="Enter email" style="width: 70%" value="<?php if(isset($_GET['Email1']))
                                {
                                    echo $_GET['Email1'];
                                } 
                                ?>" onblur="verifMail(this)" id="exampleInputEmail1" >
                                <small id="emailHelp" class="form-text text-muted">Votre adresse reste privée.</small>
                              </div>
                              <div class="form-group ">
                                <label for="pseudo">Pseudo</label>
                                <input type="text" class="form-control" name="pseudo" id="pseudo" style="width: 70%" value="<?php if(isset($_GET['pseudo']))
                                {
                                    echo $_GET['pseudo'];
                                } 
                                ?>" onblur="verifIdentifiant(this)">
                              </div>
                              <div class="form-group m-x-3">
                                <label for="exampleInputPassword1">Mot de passe</label>
                                <input type="password" class="form-control" required name="exampleInputPassword1" placeholder="Password" style="width: 70%" id="exampleInputPassword1" >
                              </div>
                              <div class="form-group m-x-3 ">
                                <label for="exampleInputPassword2">Confirmer le mot de passe</label>
                                <input type="password" class="form-control" name="exampleInputPassword2" placeholder="Password" style="width: 70%" id="exampleInputPassword2" required>
                              </div>
                              
                              <button type="submit" name="valider" id="valider" value="OK" class="btn btn-primary my-2 mx-auto"> Valider l'inscription </button>
                              <p >déjà inscris ? <a href="page_connexion.php" class="text-primary" >Connectez vous !</a></p>
                             <?php if(isset($_SESSION['erreur_mdp']))
                                {
                                 if($_SESSION['erreur_mdp']== "erreur"){ $_SESSION['erreur_mdp']= " " ?>
                                 <div class="alert alert-danger">

                                <p>Les mots de passe ne correspondent pas</p>
                                  </div>
                             <?php }
                                }?>
                              <?php if(isset($_SESSION['erreur_user']))
                                {
                                 if($_SESSION['erreur_user']== "erreur"){
                                $_SESSION['erreur_user']=" "
                                ?>
                                 <div class="alert alert-danger">

                                <p>Cet utilisateur existe déjà! Connectez vous si vous possédez un compte.</p>
                                  </div>
                             <?php }
                                }?>
                         </form>
                         <div class="col-10 col-md"><h5>Il est néccéssaire de créer un compte pour avoir accès à l'élection PING </h5>
                             <p class="font-weight-light">C'est simple et rapide! ;) </p>
                        </div>
                    </div>
                </div>

                </div>

            </div>
        </div>
    </body>
</html>