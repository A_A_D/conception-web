<?php
session_start();
include ("connexion_database.inc.php");

if(!empty($_SESSION['election'])){
    //préparation de la requête SQL
    $requete1 = $objet_PDO -> prepare('SELECT * FROM projet');
    $requete1->execute();
    while($tuple = $requete1->fetch(PDO::FETCH_ASSOC)){
        $lastline=$tuple;
    }
    $numero= $lastline['projet_id'] + 1;

    $nomOrigine = $_FILES['fichier']['name'];
    $elementsChemin = pathinfo($nomOrigine);
    $extensionFichier = $elementsChemin['extension'];
    var_dump($elementsChemin['extension']);
    $extensionsAutorisees = array("jpeg", "jpg", "png");
    if (!(in_array($extensionFichier, $extensionsAutorisees))) {
        echo "Le fichier n'a pas l'extension attendue";
    } else {
    // Copie dans le repertoire du script avec un nom
    // incluant l'heure a la seconde pres 
    $repertoireDestination = "posters/";
       // $repertoireDestination = "posters/";
    $nomDestination        = "poster".$numero.".".$extensionFichier;

    if (move_uploaded_file($_FILES['fichier']['tmp_name'],$repertoireDestination.$nomDestination)) {
            echo "Le fichier temporaire ".$_FILES['fichier']['tmp_name'].
                    " a été déplacé vers ".$repertoireDestination.$nomDestination;
        $requete2 = $objet_PDO -> prepare('insert into projet values(NULL,:projet_election,:projet_auteur,:projet_image,:projet_description,NULL)');
        $requete2->execute(array (
                              'projet_election'=> $_SESSION['election'],
                              'projet_auteur' => $_POST['auteur'],
                              'projet_image' => $repertoireDestination.$nomDestination,
                              'projet_description'=> $_POST['description']));
        header('Location:creation_sujet.php');
        } else {
            echo $_FILES['fichier']['error'];
            echo "Le fichier n'a pas été uploadé (trop gros ?) ou ".
                    "Le déplacement du fichier temporaire a échoué".
                    " vérifiez l'existence du répertoire ".$repertoireDestination;
        }
    }
}
else
    header('Location:creation_sujet.php');
?>