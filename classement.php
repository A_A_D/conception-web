<?php 
    session_start();
include('Verif_session_admin.php');
    include("connexion_database.inc.php");
    
    if(!empty($_POST)){
        $requete2 = $objet_PDO -> prepare('SELECT * FROM projet WHERE projet_election=? ORDER BY projet_votes DESC');
        $requete2->execute(array($_POST['list']));
        $tuples = $requete2->fetchAll();
        $id_election = $_POST['list'];
    }
    else{
        $requete1 = $objet_PDO -> query('SELECT * FROM projet WHERE projet_election=( SELECT MAX(id_election) FROM election) ORDER BY projet_votes DESC');
        $requete1->execute();
        $tuples = $requete1->fetchAll();
        $id_election = $tuples[0]['projet_election'];
    }
                           
    $requete3 = $objet_PDO -> query('SELECT * FROM election');
    $requete3->execute();
    $elections = $requete3->fetchAll();
?>
<!doctype html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">

            <title>Vote_ton_ping</title>
            <!-- js -->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
            <script src="js/bootstrap.min.js"></script>
            <!-- css -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/style_sheet.css">
        </head>
        
        <body  style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
               <div class="row Dblue">
                    <div class="container-fluid d-flex justify-content-center">
                        <div class="d-flex justify-content-center offset-md-1 col-8"><h3 class="my-2 text-center" style=" color: whitesmoke">Vote ton ping.</h3> </div> 
                        <div class=" justify-content-end">
                            <a href="#" class="mr-3" style="color: white;">Profil</a>
                            <a class="btn btn-custom my-3 r" href="index.php" >Déconnection</a></div>
                    </div>

                </div>
                <div class="col-12">
                    <div  class="row justify-content-center">

                        <div>
                               <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                                  <li class="nav-item">
                                    <a class="nav-link " href="page_accueil_utilisateur.php" >Accueil</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link active" href="page_poster.php">Visualisation des posters</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link active" href="creation_sujet.php">Création d'un sujet</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link active" href="gestion_election.php">Gestion de l'élection</a>
                                  </li>
                                </ul>
                        </div> 
                    </div>
                </div>
        <div class="row ml-md-2 mx-auto">
             <div class="justify-content-center shadow-sm col-md-2   col-12 h-75  Dblue my-3" style="width: 100%; border-radius: 7px">
                <article class=" mx-4 my-2 border-bottom border-white ">
                    <h4 class="px-5 m-2 py-2 d-flex justify-content-center"> <u> Profil </u></h4>
                    <div class=" mx-1  col-5 col-md-12 justify-content-center ">
                        <img src ="<?php echo $_SESSION['photo_profil']; ?>" style="height: 100px; width: 100px" alt="<?php $_SESSION['id'];?> ">
                        <h5 > <b> pseudo: </b> <?php echo $_SESSION['pseudo']. '<br />'; ?></h5>
                    </div>
                    
                </article>
                 <article class=" mx-4 my-2" style="opacity: 1"><h2 >ESIGELEC</h2>
                        <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                        </p>
                        <a href="http://www.esigelec.fr/en" style="color:white"> * visiter le site de l'ESIGELEC >> </a>
                    </article>
                </div>
             <div id="container" class=" col-12 col-md-8 container gray ml-1 mt-3 pt-5 rounded shadow  align-items-center" style="height:100%">
                  <div class="d-flex justify-content-center align-items-center border-bottom border-info">
                    <h2 class="text-dark">Classements</h2><hr>
                </div>
                    <div class="row align-items-center justify-content-center">
                            <?php 
                            try
                            {
                                $active=1;
                               include ("connexion_database.inc.php");           
                                $requete =  $objet_PDO->prepare('SELECT projet_image, projet_id FROM projet WHERE projet_election =?'); 
                                $requete->execute(array($id_election));
                            }
                            catch(Exception $e)
                            {
                                // En cas d'erreur, on affiche un message et on arrête tout
                                    die('Erreur : '.$e->getMessage());
                            }
                            ?>
                        
                            <div class="pl-5 col-12 col-md-4 py-2">
                                <h6 class="text-dark"> Posters de l'élection n° <?php echo $id_election ;?></h6> 
                                <div class="card bg-dark text-white rounded border  border-right border-dark " style="height:430px ; width:auto">
                                    <div id="carouselEx" class="carousel slide " data-ride="carousel">
                                      <div class="carousel-inner">
                                          <?php 
                                          $i=0;
                                          while($poster = $requete ->fetch())
                                          { ?>


                                        <div class="carousel-item <?php if($i==0){
                                              echo active;
                                          $i=1; } ?>">
                                          <img class=" item d-block w-100" src="<?php echo $poster['projet_image']; ?>" alt="<?php echo $poster['projet_id']; ?>" style="height:100%">
                                            <div class="card-img-overlay">
                                            <div class="carousel-caption d-none d-md-block">
                                              <span class="badge  badge-dark card-text">Projet <?php echo $poster['projet_id'];
                                                  ?></span>
                                            </div>
                                            </div>
                                        </div>
                                           <?php }
                                          ?>
                                      </div>
                                    </div>
                                </div>
                            </div>
                 <div class="d-flex justify-content-center col-12 col-md-8 ">
                    <div class="d-flex justify-content-center  pl-md-5 pt-3 rounded">
                        <table class="table" >
                          <thead class="thead-black" style="color:#13618E; background-color:whitesmoke;">
                            <tr>
                              <th scope="col">Rang</th>
                              <th scope="col">Poster</th>
                              <th scope="col">Groupe Ping</th>
                              <th scope="col">nombre de votes</th>
                            </tr>
                          </thead>
                          <tbody style="color:whitesmoke;  background-color:rgba(19,97,142,0.6);">
                              <?php 
                            $i=1;
                             foreach ($tuples as $tuple): ?>
                             <tr>
                               <th scope="row"> <?= $i++ ?></th>
                                <td>Projet n° <?= $tuple['projet_id'] ?></td>
                                 <td><?= $tuple['projet_auteur'] ?></td>
                                 <td><?= $tuple['projet_votes'] ?></td>
                              </tr>
                             <?php endforeach; ?>

                          </tbody>
                        </table>
                    </div>
                </div>                
        </div>
           <hr>
                 
        <form action="classement.php" method="post">
                <div class="col-6">
                    <label for="select"> Veuillez choisir une élection : </label>
                     <select name="list" class="form-control " id="select">
                        <?php   
                        foreach ($elections as $election): ?>                             
                            <option><?= $election['id_election'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                 <hr>
                <input type="submit" class="btn btn-customfull">
                <hr>
        </form>
    </div>
</div>
</body>
</html>