
function verifIdentifiant(champ)
{
    var controlChamp= document.getElementById('id-control');
   if(champ.value.length < 2)
   {
       controlChamp.textContent="Votre identifiant doit avoir au moins 2 caractères";
        return false;
   }
   else if( champ.value.length > 10)
   {
    controlChamp.textContent="Votre identifiant doit avoir au plus 10 caractères";
   }
   else 
   {
        controlChamp.textContent=" ";
        return true;
   }
}
function verifMail(champ)
{
    var controlChamp=document.getElementById('mail-control');
    var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
   if(!regex.test(champ.value))
   {
      controlChamp.textContent = "Adresse mail non valide";
      return false;
   }
   else
   {
        controlChamp.textContent=" ";
        return true;
   }
}
function verifMdp(champ)
{
    var controlChamp=document.getElementById('mdp-control');
   if(champ.value.length < 6)
   {
    controlChamp.textContent = "Votre mot de passe doit avoir au moins 6 caractères";
        return false;
   }
   else 
   {
         controlChamp.textContent=" ";
        return true;
   }
}
function verifConfirmMdp(champ)
{
    var controlChamp=document.getElementById('confirmMdp-control');
   if(champ.localeCompare(document.forms[0].motDePasse)!=0)
   {
    controlChamp.textContent = "Veuillez saisir le même mot de passe";
        return false;
   }
   else 
   {
        controlChamp.textContent=" ";
        return true;
   }
}
function verifForm(f)
{
    var controlForm= document.getElementById('form-control');
    var pseudoOk = verifIdentifiant(f.identifiant);
    var mailOk = verifMail(f.adressemail);
    var ageOk = verifMdp(f.motDePasse);
   
   if(pseudoOk && mailOk && ageOk){
        controlChamp.textContent=" ";
        return true;
   }
   else
   {
        controlForm.textContent= "Veuillez remplir correctement tous les champs";
        return false;
   }
}

function gBox(nbCheck) {
    if(document.getElementById(nbCheck).checked == true) {
        document.getElementById('formulaire1').submit();
    }
    else {
        alert('Vous devez cocher la case !');
    }
}