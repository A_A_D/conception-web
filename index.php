<?php 
session_start();
session_destroy();
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
  <title>Vote_ton_ping</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_sheet.css">
</head>
    <body  style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
        <div class="row  Dblue">
            <div class="container-fluid d-flex justify-content-center">
                <div class="d-flex justify-content-center offset-md-1 col-8"><h4 class="my-3 text-center" style=" color: whitesmoke">Vote ton ping.</h4> </div> 
                <div class=" justify-content-end"><a class="btn btn-custom my-3" href="page_connexion.php" >connexion</a></div>
            </div>
           
        </div>
        <div class="col-12 mx-auto">
            <div  class="row justify-content-center">

                <div>
                       <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                          <li class="nav-item">
                            <a class="nav-link active " href="page_connexion.php" >Connexion</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link " href="page_inscription.php">Inscription</a>
                          </li>
                        </ul>
                </div> 
            </div>
            <div  class="row">
                <div class="  shadow-sm col-md-2  col-sm-12 col-12 h-75  Dblue ml-2 my-3" style="width: 100%; border-radius: 7px">
                    <article class=" mx-4 my-2" style="opacity: 1"><h2 >ESIGELEC</h2>
                        <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                        </p>
                        <a href="http://www.esigelec.fr/en" style="color:white"> * visiter le site de l'ESIGELEC >> </a>
                    </article>
                </div>
                
                <div id="container" class=" col-12 col-md-9 container gray ml-2 mt-3  mb-1 rounded shadow" style="height: 100%">
                    <div class="d-flex">
                        <div class="row align-items-center">
                             <img class="col-12 col-md-4 my-3 justify-content-center rounded" src="nesa-by-makers-kwzWjTnDPLk-unsplash.jpg" alt="image ping">
                             <article class="col-12 col-md-8   my-2 text-center">
                                <h1>Le <b>PING</b> qu'est ce que c'est ?</h1><br />
                                <p>PING signifie <b>projet ingénieur</b></p>
                                <p>il s'agit d'un projet fourni par un client et dont le but est de former les étudiants de troisieme année à la gestion et à la réalisation d'un projet technique dans tous ses aspects: budjet, deadlines,humanitaires...</p>
                                <p>vous pouvez décider de supporter votre équipe favorite en votant pour son projet!</p>
                                <a class="btn btn-primary b my-3 " href="page_inscription.php">commencez maintenant ! </a>
                            </article>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </body>
</html>