<?php 
session_start(); 
include('verif_session.php');

?>
<!doctype html>
<html lang="fr">
        <head>
          <meta charset="utf-8">
	  <meta name="viewport" content="width-device-width, initial-scale=1, shrink-to-fit=no">
          <title>Vote_ton_ping</title>
            <!-- css -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/style_sheet.css">
        </head>
            <body style="background-image: url(martin-adams-a_PDPUPuNZ8-unsplash.jpg)">
               <div class="row Dblue">
                    <div class="container-fluid d-flex justify-content-center">
                        <div class="d-flex justify-content-center offset-md-1 col-8"><h3 class="my-2 text-center" style=" color: whitesmoke">Vote ton ping.</h3> </div> 
                        <div class=" justify-content-end">
                            <a href="#" class="mr-3" style="color: white;">Profil</a>
                            <a class="btn btn-custom my-3 r" href="index.php" >Déconnection</a></div>
                    </div>
           
        </div>
       <div class="col-12">
                    <div  class="row justify-content-center">

                        <div>
                               <ul id="navi" class="nav border-bottom " style="font-size: 14pt; ">
                                  <li class="nav-item">
                                    <a class="nav-link active " href="page_accueil_utilisateur.php" >Accueil</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link disabled" href="page_poster.php" >Visualisation des posters</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?>" href="creation_sujet.php">Création d'un sujet</a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link <?php if($_SESSION['statut']!=0){ ?>
                                              active
                                    <?php } else { ?> disabled <?php } ?>" href="gestion_election.php">Gestion de l'élection</a>
                                  </li>
                                </ul>
                        </div> 
                    </div>
         </div>
            <div  class="row ml-md-2 mx-auto">
                 <div class="   justify-content-center shadow-sm col-md-2   col-12 h-75  Dblue my-3 ml-2 " style="width: 100%; border-radius: 7px">
                <article class=" mx-4 my-2 border-bottom border-white ">
                    <h4 class="px-5 m-2 py-2 d-flex justify-content-center"> <u> Profil </u></h4>
                    <div class=" mx-1  col-5 col-md-12 justify-content-center ">
                        <img src ="<?php echo $_SESSION['photo_profil']; ?>" style="height: 100px; width: 100px" alt="<?php echo $_SESSION['user_id']; ?>">
                        <h5 > <b> pseudo: </b> <?php echo $_SESSION['pseudo']; echo "<br />"; ?></h5>
                    </div>
                    
                </article>
                    <article class=" mx-4 my-2" style="opacity: 1"><h2 >ESIGELEC</h2>
                        <p class="justify-text">L’ESIGELEC est une école française d’ingénieurs basée à Rouen et créée en 1901. Elle fait partie des meilleures institutions académiques françaises connues sous le nom de grandes écoles spécialisées dans l’ingénierie et les sciences et est une institution de niveau universitaire dotée du statut particulier de Grands établissements.
                        </p>
                        <a href="http://www.esigelec.fr/en" style="color:white"> * visiter le site de l'ESIGELEC >> </a>
                    </article>
                </div>
           <div id="container" class=" col-12 col-md-9 container gray ml-1 mt-3  rounded shadow mb-1" style="height: 100%">
                <div class="row justify-content-center">
                    <h2 class=" my-4 py-4">Projets en ligne</h2><br />
                </div>
                <?php  try { 
                       include ("connexion_database.inc.php");
                $active= 1;
                $requete_2 = $objet_PDO->prepare('SELECT COUNT(id_election) AS nb FROM election WHERE statut =  :active');
                $requete_2->bindParam(':active', $active, PDO::PARAM_INT);
                $requete_2->execute();
                $election = $requete_2->fetch();
                if($election['nb'] >=1)
                {
                $requete =  $objet_PDO->prepare('SELECT * FROM projet WhERE projet_election IN (SELECT id_election FROM election WHERE statut = :active)'); 
               $requete->bindParam(':active', $active, PDO::PARAM_INT);
                $requete->execute();?>
                    <div class="row">
                        <?php while($poster = $requete ->fetch()){ 
                        $id= $poster['projet_id']; $election= $poster['projet_election']; $auteur=$poster['projet_auteur']; $image=  $poster['projet_image']; $description=  $poster['projet_description'];
                    
                        ?>
                        <div class=" col-12 col-md-4" style="width: auto; height: auto;">
                            <figure class="card shadow col-md-9 col-xs-12  offset-md-2">
                                <div class="card-Title"><h5>projet <?php echo $poster['projet_id'];  ?> </h5></div>
                                 <a href="page_presente_poster.php?id=<?php echo $id; ?>&amp;election=<?php echo $election; ?>&amp;auteur=<?php echo $auteur; ?>&amp;image=<?php echo $image; ?>&amp;description=<?php echo $description; ?>" > <img src="<?php echo $poster['projet_image']; ?>" alt=<?php echo "poster". $poster['projet_id']; ?> class="col-md-12 my-3"></a>
                                <div class="card-body ">
                                <p class="card-text"><b><u>description du projet: </u></b><br><?php echo $poster['projet_description']; ?></p>
                              </div>
                            </figure>                 
                        </div>
                        <?php } ?>
                         
                    </div>
                   
                    
                                  <?php }
                                        else
                                        {?>
                                        <div class="alert alert-info" role="alert" style="width:100%">
                                          <div class="row"> 
                                              <h4 class="alert-heading pl-4 pr-4">Aucune élection en cours.</h4>
                                              <a class="btn btn-customfull a" href="classement.php" >Afficher le classement</a> 
                                          </div>
                                            <hr>
                                          Toutes les élections sont cloturées pour le moment, vous pouvez à présent <a href="classement.php" class="alert-link">consulter les classements</a>. 
                                        </div>
                                      <?php  }
                                 ?>
               <div class="d-flex justify-content-end"> <a href="#navi" class="btn btn-customfull a mb-3">retourner au début de page</a></div>
               <?php 
                                } 
               
                            catch(Exception $e)
                            {
                                // En cas d'erreur, on affiche un message et on arrête tout
                                    die('Erreur : '.$e->getMessage());
                            }
                            ?>
                </div>    
    </div>
</body>
</html>